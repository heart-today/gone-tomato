extends Control


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	var cursor = load("res://demo/images/cursor.png")
	assert(cursor)
	
	Input.set_custom_mouse_cursor(cursor)
	
	BackgroundMusic.play()


func _on_Exit_pressed():
	get_tree().quit()


func _on_Play_pressed():
#	get_tree().change_scene("res://demo/scenes/main/Gameplay.tscn")
#	get_tree().change_scene("res://BoxRoom.tscn")
#	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().change_scene("res://split_screen.tscn")

func _on_Credits_pressed():
	get_tree().change_scene("res://demo/scenes/main/Credits.tscn")


func _on_Settings_pressed():
	$Settings.show()


func _on_Intro_pressed():
	get_tree().change_scene("res://IntroControl.tscn")
