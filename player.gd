extends KinematicBody

# Moves the player

export(int, 1, 2) var player_id = 1
export(float) var walk_speed = 20.0

onready var player1 = $"../Player1"
onready var player2 = $"../Player2"

func _ready():
	load_position()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func load_position():
	var pos = goat_state.get_value("player%s_pos" % player_id)
	if pos != null:
		self.translation = pos
	
	var rot = goat_state.get_value("player%s_rot" % player_id)
	if rot != null:
		self.rotation = rot
			
func save_position():
	goat_state.set_value("player%s_pos" % player_id, self.translation)
	goat_state.set_value("player%s_rot" % player_id, self.rotation)
	
var direction = Vector3.BACK

const SMOOTH_SPEED = 10.0

func _physics_process(delta):
	var velocity = Vector3.ZERO
	velocity.z = -Input.get_action_strength("move_up_player" + str(player_id))
	velocity.z += Input.get_action_strength("move_down_player" + str(player_id))
	velocity.x = -Input.get_action_strength("move_left_player" + str(player_id))
	velocity.x +=  Input.get_action_strength("move_right_player" + str(player_id))

	# Rotate them around towards where they are heading
	if velocity.length_squared() > .1:
		var direction = velocity
		
		# Sadly, models are offset by PI/2 and no time to correct! ;-)
		rotation.y = lerp_angle(rotation.y, PI/2 + atan2(-direction.x, -direction.z), delta * SMOOTH_SPEED)
		#rotation.y = atan2(-direction.x, -direction.z)

	move_and_slide(velocity.normalized() * walk_speed)
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision != null:
			var collider_name = collision.collider.name
			if collider_name.begins_with("Wall"):
				change_scene("res://BoxRoom.tscn")

func change_scene(scene):
	goat_state.set_value("player_id", player_id)
	player1.save_position()
	player2.save_position()
	get_tree().change_scene(scene)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
func _input(_event):
	if Input.is_action_just_pressed("goat_dismiss"):
		change_scene("res://demo/scenes/main/MainMenu.tscn")
