# Heart Today Gone Tomato

A story of cardiovascular depression and marinara.

## Introduction

Grey Heart was lonely. 

He had suffered heartbreak once before, and it left him bereft of life, and meaning in his eyes.

Cracked, gray, and utterly depressed, the only semblance of joy he still had was his vintage Green-Onion Day vinyl records and his odd yet warming love for tomato based food products. 

After living day to day with no sense of self-importance, Heart decided to set off on his journey to find the source of his beloved tomato based food products, the sweet “Cheri Tomato” after which the foods were named.

-----

Cheri Tomato was tired. 

She put her food products out there in order to try and find someone, anyone passionate and caring enough to love her. 

Every ounce of flavor had gallons of passion poured into it, and every can of tomato soup or tube of tomato paste was a serenade to her marinade-crafting dream lover, wherever they might be.

She wanders the streets of Paris at night, hoping to be found, recognized, appreciated.

-----

Will these two wayward loves, one blind and the other directionless, find each other? 

Or will they find themselves in kahoots with fate, stuck in the transient state of searching without ever getting the heart-pounding crescendo that only existed in fairy tales up until now?

## Intentions

- [x] To have fun and make friends at [Godot Wild Jam #30](https://godotwildjam.com/)
- [x] Submit onto [itch.io](https://hanumanjiyogi.itch.io/heart-today-gone-tomato)
- [ ] #1
- [ ] Integrate with [Logos](https://gitlab.com/their-temple/logos)


## Features

...not a whole lot...LOL

## Demo

Yet to be done...see #1 (Hey!  It rhymes!)

## Installation

Download from the [itch.io page](https://hanumanjiyogi.itch.io/heart-today-gone-tomato)

## License
