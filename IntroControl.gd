extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	BackgroundMusic.stop()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_VideoPlayer_finished():
	get_tree().change_scene("res://demo/scenes/main/MainMenu.tscn")

func _input(_event):
	if Input.is_action_just_pressed("goat_dismiss"):
		get_tree().change_scene("res://demo/scenes/main/MainMenu.tscn")
